import git  # забыл про requirements.txt и долго не мог понять, что не так с библиотекой
import time
# https://blog.balasundar.com/automate-git-operations-using-python


def git_sync_ckecker(work_dir, manual):
    print("[INFO] Запуск срипта проверки синхронизации с удаленным репозиторием git")
    print("[INFO] поиск локального репозитория")
    try:
        repo = git.Repo.init(work_dir)
        print("[OK] репозиторий найден")
    except:
        print("[ERROR] неправильный путь до директории!")
        exit(1)
    while True:
        remote_head_hash = ''
        try:
            print("[INFO] пробуем подключиться к remote и выдернуть хэш HEAD")
            # branch = repo.active_branch #я хочу сделать автовыбор ветки, но пока не понял, как заменять переменной имя метода
            remote_head_hash = repo.remotes.origin.refs.main.commit
            print("[OK] подключение успешно")
            if remote_head_hash != repo.head:
                print('[INFO] Локальный репозиторий устарел')
            if repo.is_dirty():
                print('[INFO] Локальные изменения не синхронизированы')
                if manual: #хотим что-то изменить
                    if repo.untracked_files:
                        gitignore(repo) #редактирование гитигнора
                    print('Хотите выполнить синхронизацию? (y/n)')
                    if input() in ("yes", "y", "н", "Y", "Н"):
                        sync(repo) #ребейз
                        if not repo.is_dirty():
                            print('[INFO] Репозиторий синхронизирован!')
                        else:
                            print("[ERROR] что-то пошло не так")
            else:
                print("[OK] Репозиторий синхронизирован")
        except:
            print('[ERROR] Нет доступа к удаленному репозиторию!')
        time.sleep(60)


def sync(repo): #очевидно ошибка здесь, но где?
    g = git.cmd.Git("../")
    g.pull(rebase=True)


def gitignore(repo):
    if repo.untracked_files:
        print(f'Неотслеживаемые файлы: {repo.untracked_files}')
        print('добавить все файлы в gitignore (1) или выбрать отдельные файлы (2)?')
        answer = input()
        if answer == '1':
            for i in repo.untracked_files:
                with open('../.gitignore', 'a') as the_file:
                    the_file.write(f'{i}\n')
        else:
            for i in repo.untracked_files:
                print(f'Добавить файл {i} в .gitignore? (y/n)')
                if input() in ("yes", "y", "н", "Y", "Н"):
                    with open('../.gitignore', 'a') as the_file:
                        the_file.write(f'{i}\n')
        print(repo.untracked_files)
    else:
        print("[INFO] нет неотслеживаемых файлов")


# repo = git.Repo.init('../')
# repo.git.checkout('main')
# commits = repo.iter_commits('--all', max_count=5)
# for commit in commits:
#     print(commit.hexsha)
# # remotes = repo.remotes.origin.refs.main._get_commit())
# print(repo.remote().refs.main)
# # for i in git.Remote.iter_items(repo=repo):
# remotes = repo.remotes.origin.refs.main.commit
# # print(repo.remotes.origin.refs.main.commit.tree)
#
# branch = repo.active_branch
# # remote_head_hash = repo.remotes.origin.refs.branch.commit
# print(remote_head_hash)