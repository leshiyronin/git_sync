FROM ubuntu:24.04

RUN apt update
RUN apt upgrade -y
RUN apt install -y python3 python3-venv
RUN apt install python3-pip -y
# RUN apt install -y python3.11 python3-pip
COPY requirements.txt /opt/app/requirements.txt
WORKDIR /opt/app
RUN python3 -m venv .venv/
RUN .venv/bin/pip3 install -r requirements.txt
COPY main.py main.py
COPY src src
CMD ["python3", "main.py"]
