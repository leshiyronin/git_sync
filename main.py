# main.py
from src import git_sync
import sys


def main(work_dir, manual=False):
    git_sync.git_sync_ckecker(work_dir, manual)


if __name__ == "__main__":
    manual = False
    work_dir = str(sys.argv[1])
    try:
        manual = bool(sys.argv[2])
    except:
        pass
    # print(work_dir, sys.argv[2], manual)
    main(work_dir, manual)
